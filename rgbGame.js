var easy = document.querySelector(".easy");
var hard = document.querySelector(".hard");
var colors = generateRandomColors(6);
easy.addEventListener("click", function () {
    afterGameMessage.textContent = "";
    colors = generateRandomColors(3);
    randomColorIndex = generateRandomNumber();
    correctColor = colors[randomColorIndex];
    for (var i = 0; i < colors.length; i++) {
        box[i].style.backgroundColor = colors[i];
    }
    for(var i = colors.length ; i < box.length ; i++){
         box[i].style.backgroundColor = 'black';
    }
    document.querySelector(".heading").style.backgroundColor = '#2980b9';
    document.querySelector("#number1").textContent = correctColor;
});
hard.addEventListener("click", function () {
    afterGameMessage.textContent = "";
    colors = generateRandomColors(6);
    randomColorIndex = generateRandomNumber();
    correctColor = colors[randomColorIndex];
    for (var i = 0; i < colors.length; i++) {
        box[i].style.backgroundColor = colors[i];
    }
    document.querySelector(".heading").style.backgroundColor = '#2980b9';
    document.querySelector("#number1").textContent = correctColor;
});
var box = document.getElementsByClassName("Box");
var randomColorIndex = generateRandomNumber();
var correctColor = colors[randomColorIndex];
var number = document.querySelector("#number1");
var afterGameMessage = document.querySelector("#afterGameMessage");
var newColors = document.querySelector(".newColors");
newColors.addEventListener("click", function () {
    afterGameMessage.textContent = "";
    colors = generateRandomColors(6);
    randomColorIndex = generateRandomNumber();
    correctColor = colors[randomColorIndex];
    for (var i = 0; i < colors.length; i++) {
        box[i].style.backgroundColor = colors[i];
    }
    document.querySelector(".heading").style.backgroundColor = '#2980b9';
    document.querySelector("#number1").textContent = correctColor;
});
number.textContent = correctColor;
for (var i = 0; i < colors.length; i++) {
    box[i].style.backgroundColor = colors[i];
    box[i].addEventListener("click", function () {
        if (this.style.backgroundColor === correctColor) {

            afterGameMessage.textContent = "Correct!";
            changeBoxesAfterSuccess();
        } else {
            this.style.backgroundColor = 'black';
            afterGameMessage.textContent = "Try Again."
        }
    });
}

function changeBoxesAfterSuccess() {
    for (var i = 0; i < colors.length; i++) {
        box[i].style.backgroundColor = correctColor;
    }
    document.querySelector(".heading").style.backgroundColor = correctColor;
}

function generateRandomNumber() {
    var randomNumber = Math.floor(Math.random() * colors.length);
    return randomNumber;
}

function generateRandomColors(num) {
    //   ["rgb(255, 0, 0)", "rgb(0, 255, 0)", "rgb(0, 0, 255)", "rgb(255, 255, 0)", "rgb(0, 255, 255)",
    //              "rgb(255, 0, 255)"];
    var cArr = [];
    for (var i = 0; i < num; i++) {
        var str = randomColor();
        cArr[i] = str;
    }

    return cArr;
}

function randomColor() {
    var r = Math.floor(Math.random() * 256);
    var g = Math.floor(Math.random() * 256);
    var b = Math.floor(Math.random() * 256);

    return "rgb(" + r + ", " + g + ", " + b + ")";
}
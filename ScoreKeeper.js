// Enter amount into number box and it should show next to 'Playing to: '

var num = document.querySelector("input");
max = document.querySelector(".max");
num.addEventListener("change", function(){
    max.textContent = num.value;
    resetEvent();
})


// Make player1, player2 and reset buttons responsive.

var player1Button = document.querySelector(".player1");
var player2Button = document.querySelector(".player2");
var resetButton = document.querySelector(".reset");
var score1 = 0;
var score2 = 0;
var gameOver = false;
var player1Score = document.querySelector(".player1Score");
var player2Score = document.querySelector(".player2Score");
player1Button.addEventListener("click",function(){
    if(!gameOver){    
    score1++;
    player1Score.textContent = score1;
        if(score1 === Number(num.value)){
            player1Score.style.color = "green";
            gameOver = true;   
        }
    }
})

player2Button.addEventListener("click",function(){
    if(!gameOver){    
    score2++;
    player2Score.textContent = score2;
        if(score2 === Number(num.value)){
            player2Score.style.color = "green";
            gameOver = true;
        }
    }
})
function resetEvent(){
    score1 = 0;
    score2 = 0;
    player1Score.textContent = 0;
    player2Score.textContent = 0;
    player1Score.style.color = "black";
    player2Score.style.color = "black";
    gameOver = false;
}
resetButton.addEventListener("click",function(){
    num.value="";
    max.textContent="";
    resetEvent();
});
/*
    Goal is to provide three options to the user.
    1. Add a task.
    2. List all tasks.
    3. Quit the application.
*/

var options = ["new", "list", "quit"]
var tasks = []

// Adds new task
function newTask(newTask) {
    tasks.push(newTask)
}

// Lists tasks in the list
function listTasks() {
    if (tasks.length === 0)
        return "There are no tasks in your list of todos."
    for (var taskId = 0; taskId < tasks.length; taskId++)
        console.log(tasks[taskId])
}

// quits app
function quitApp() {
    tasks = []
    return "Sorry to see you go."
}

function app() {
    var end = false;
    while (end === false) {
        var selectedOption = prompt("What would you like to do?");
        if(selectedOption === false || selectedOption === null){ // canceled prompt
            end = true;
            continue;
        }
        switch (selectedOption) {
            case "new":
                var taskToBeAdded = prompt("Please enter your task.")
                if (taskToBeAdded.length === 0) {
                    alert("Do not leave text empty.")
                    break;
                }
                newTask(taskToBeAdded)
                break;

            case "list":
                listTasks()
                break;

            case "quit":
                quitApp();
                end = true;
                break;
            default:
                alert("Please enter only one of the three options.")
                break;
        }
    }
}

app();
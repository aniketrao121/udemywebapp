function factorial(num){
    // assume num = 4
    // result = 4 * 3 * 2 * 1
    // i.e., result = num * (num-1) * (num-2) * (num-3)
    if(isNaN(num))
        return 'Please enter a valid number'
    var result = 1
    while(num > 0){
        result = result * num
        num = num - 1
    }
    return result
}

// convert '-' to '_' 
function kebabToSnake(sentence){
    return sentence.split('-').join('_')
}

//var number = prompt("Please enter a number to find the factorial of")
//var fact = factorial(number)
//alert("Factorial is " + fact)

console.log(kebabToSnake("dog-cat-puppy-horse"))